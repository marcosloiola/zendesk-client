/**
 * 
 */
package com.movile.zendesk.enums;

/**
 * @author marcosloiola1
 * 
 */
public enum ZendeskAttributes {

	ANDROID_TOKENS(21383180),
	APP_VERSION(21223074),
	COMMENT(21383200),
	COUNTRY(21255435),
	DEVICE(21223064),
	DEVICE_ID(21211589),
	EMAIL(21383190),
	IOS_TOKENS(21211609),
	FACEBOOK_ID(21222914),
	LANGUAGE(21255445),
	LIKE(21255455),
	MODEL(21383090),
	NAME(21383060),
	NPS(21383210),
	ORIGIN(21383070),
	SYSTEM_NAME(21255425),
	SYSTEM_VERSION(21383650),
	TIMEZONE(21383100),
	USER_AGENT(21211599),
	USER_ID(21211619);

	private Number zendeskId;

	private ZendeskAttributes(Number zendeskId) {
		this.zendeskId = zendeskId;
	}

	public Number getZendeskId() {
		return zendeskId;
	}

}
