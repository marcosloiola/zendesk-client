package com.movile.zendesk.bean;

public class ZDTicketCustomFields {
	private Number id;
	private Object value;

	public ZDTicketCustomFields() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ZDTicketCustomFields(Number id, Object value) {
		super();
		this.id = id;
		this.value = value;
	}

	public Number getId() {
		return this.id;
	}

	public void setId(Number id) {
		this.id = id;
	}

	public Object getValue() {
		return this.value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
}
