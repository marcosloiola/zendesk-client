package com.movile.zendesk.bean;

import java.util.ArrayList;
import java.util.List;

public class ZDTicket {

	private boolean has_incidents;
	private Number assignee_id;
	private Number forum_topic_id;
	private Number group_id;
	private Number id;
	private Number organization_id;
	private Number problem_id;
	private Number requester_id;
	private Number submitter_id;
	private String type;
	private String updated_at;
	private String url;
	private String created_at;
	private String description;
	private String due_at;
	private String external_id;
	private String priority;
	private String recipient;
	private String status;
	private String subject;
	private List<ZDTicketCustomFields> custom_fields;
	private List<String> collaborator_ids;
	private List<Number> sharing_agreement_ids;
	private List<String> tags;
	private ZDTicketSatisfactionRating satisfaction_rating;
	private ZDUser requester;
	private ZDTicketVia via;

	public ZDTicket() {
		super();
		this.custom_fields = new ArrayList<ZDTicketCustomFields>();
		this.collaborator_ids = new ArrayList<String>();
		this.tags = new ArrayList<String>();
		this.sharing_agreement_ids = new ArrayList<Number>();
	}

	public boolean isHas_incidents() {
		return has_incidents;
	}

	public void setHas_incidents(boolean has_incidents) {
		this.has_incidents = has_incidents;
	}

	public Number getAssignee_id() {
		return assignee_id;
	}

	public void setAssignee_id(Number assignee_id) {
		this.assignee_id = assignee_id;
	}

	public Number getForum_topic_id() {
		return forum_topic_id;
	}

	public void setForum_topic_id(Number forum_topic_id) {
		this.forum_topic_id = forum_topic_id;
	}

	public Number getGroup_id() {
		return group_id;
	}

	public void setGroup_id(Number group_id) {
		this.group_id = group_id;
	}

	public Number getId() {
		return id;
	}

	public void setId(Number id) {
		this.id = id;
	}

	public Number getOrganization_id() {
		return organization_id;
	}

	public void setOrganization_id(Number organization_id) {
		this.organization_id = organization_id;
	}

	public Number getProblem_id() {
		return problem_id;
	}

	public void setProblem_id(Number problem_id) {
		this.problem_id = problem_id;
	}

	public Number getRequester_id() {
		return requester_id;
	}

	public void setRequester_id(Number requester_id) {
		this.requester_id = requester_id;
	}

	public Number getSubmitter_id() {
		return submitter_id;
	}

	public void setSubmitter_id(Number submitter_id) {
		this.submitter_id = submitter_id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDue_at() {
		return due_at;
	}

	public void setDue_at(String due_at) {
		this.due_at = due_at;
	}

	public String getExternal_id() {
		return external_id;
	}

	public void setExternal_id(String external_id) {
		this.external_id = external_id;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public List<ZDTicketCustomFields> getCustom_fields() {
		return custom_fields;
	}

	public void setCustom_fields(List<ZDTicketCustomFields> custom_fields) {
		this.custom_fields = custom_fields;
	}

	public List<String> getCollaborator_ids() {
		return collaborator_ids;
	}

	public void setCollaborator_ids(List<String> collaborator_ids) {
		this.collaborator_ids = collaborator_ids;
	}

	public List<Number> getSharing_agreement_ids() {
		return sharing_agreement_ids;
	}

	public void setSharing_agreement_ids(List<Number> sharing_agreement_ids) {
		this.sharing_agreement_ids = sharing_agreement_ids;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public ZDTicketSatisfactionRating getSatisfaction_rating() {
		return satisfaction_rating;
	}

	public void setSatisfaction_rating(ZDTicketSatisfactionRating satisfaction_rating) {
		this.satisfaction_rating = satisfaction_rating;
	}

	public ZDUser getRequester() {
		return requester;
	}

	public void setRequester(ZDUser requester) {
		this.requester = requester;
	}

	public ZDTicketVia getVia() {
		return via;
	}

	public void setVia(ZDTicketVia via) {
		this.via = via;
	}

	public void addCustomField(ZDTicketCustomFields customField) {
		this.custom_fields.add(customField);
	}

	public void addCustomField(Number id, Object value) {
		this.custom_fields.add(new ZDTicketCustomFields(id, value));
	}

	@Override
	public String toString() {
		return "ZDTicket [assignee_id=" + assignee_id + ", collaborator_ids=" + collaborator_ids + ", created_at=" + created_at + ", custom_fields=" + custom_fields + ", description=" + description + ", due_at=" + due_at + ", external_id=" + external_id + ", forum_topic_id=" + forum_topic_id + ", group_id=" + group_id + ", has_incidents=" + has_incidents + ", id=" + id + ", organization_id=" + organization_id + ", priority=" + priority + ", problem_id=" + problem_id + ", recipient=" + recipient + ", requester_id=" + requester_id + ", satisfaction_rating=" + satisfaction_rating + ", sharing_agreement_ids=" + sharing_agreement_ids + ", status=" + status + ", subject=" + subject + ", submitter_id=" + submitter_id + ", tags=" + tags + ", type=" + type + ", updated_at=" + updated_at + ", url=" + url + ", via=" + via + "]";
	}

}
