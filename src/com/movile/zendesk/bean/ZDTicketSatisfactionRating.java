package com.movile.zendesk.bean;


public class ZDTicketSatisfactionRating{
   	private String comment;
   	private Number id;
   	private String score;

 	public String getComment(){
		return this.comment;
	}
	public void setComment(String comment){
		this.comment = comment;
	}
 	public Number getId(){
		return this.id;
	}
	public void setId(Number id){
		this.id = id;
	}
 	public String getScore(){
		return this.score;
	}
	public void setScore(String score){
		this.score = score;
	}
}
