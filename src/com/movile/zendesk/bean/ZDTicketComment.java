/**
 * 
 */
package com.movile.zendesk.bean;

/**
 * @author marcosloiola1
 * 
 */
public class ZDTicketComment {

	private String body;

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

}
