/**
 * 
 */
package com.movile.zendesk.bean.api;

import com.movile.zendesk.bean.ZDUser;

/**
 * @author marcosloiola1
 * 
 */
public class ZDUserCreateRequest {

	private ZDUser user;

	public ZDUser getUser() {
		return user;
	}

	public void setUser(ZDUser user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "UserCreateRequest [user=" + user + "]";
	}

}
