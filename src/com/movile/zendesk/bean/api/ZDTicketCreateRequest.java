/**
 * 
 */
package com.movile.zendesk.bean.api;

import com.movile.zendesk.bean.ZDTicket;

/**
 * @author marcosloiola1
 * 
 */
public class ZDTicketCreateRequest {

	private ZDTicket ticket;

	public ZDTicket getTicket() {
		return ticket;
	}

	public void setTicket(ZDTicket ticket) {
		this.ticket = ticket;
	}

}
