/**
 * 
 */
package com.movile.zendesk.test;

import java.util.ArrayList;
import java.util.List;

import com.movile.zendesk.ZendeskClient;
import com.movile.zendesk.bean.ZDTicket;
import com.movile.zendesk.bean.ZDTicketCustomFields;
import com.movile.zendesk.bean.ZDUser;
import com.movile.zendesk.bean.api.ZDTicketCreateRequest;
import com.movile.zendesk.bean.api.ZDUserCreateRequest;
import com.movile.zendesk.enums.ZendeskAttributes;

/**
 * @author marcosloiola1
 * 
 */
public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		ZDUser user = new ZDUser();
		user.setEmail("marcos.loiol@movile.com");
		user.setName("Marcos Loiola");

		ZDUserCreateRequest userCreateRequest = new ZDUserCreateRequest();
		userCreateRequest.setUser(user);

		System.out.println(ZendeskClient.createUser(userCreateRequest));

		ZDTicket ticket = new ZDTicket();

		ticket.setRequester(user);

		ticket.setSubject("Assunto3 via api: " + user.getName() + " - " + user.getEmail());

		ticket.setDescription("Descricao3 via api");

		List<ZDTicketCustomFields> customFields = new ArrayList<ZDTicketCustomFields>();

		ZDTicketCustomFields cf = new ZDTicketCustomFields();
		cf.setId(ZendeskAttributes.ANDROID_TOKENS.getZendeskId());
		cf.setValue("android_token");
		customFields.add(cf);

		cf = new ZDTicketCustomFields();
		cf.setId(ZendeskAttributes.APP_VERSION.getZendeskId());
		cf.setValue("APP_VERSION");
		customFields.add(cf);

		cf = new ZDTicketCustomFields();
		cf.setId(ZendeskAttributes.COMMENT.getZendeskId());
		cf.setValue("COMMENT");
		customFields.add(cf);

		cf = new ZDTicketCustomFields();
		cf.setId(ZendeskAttributes.COUNTRY.getZendeskId());
		cf.setValue("COUNTRY");
		customFields.add(cf);

		cf = new ZDTicketCustomFields();
		cf.setId(ZendeskAttributes.DEVICE.getZendeskId());
		cf.setValue("DEVICE");
		customFields.add(cf);

		cf = new ZDTicketCustomFields();
		cf.setId(ZendeskAttributes.DEVICE_ID.getZendeskId());
		cf.setValue("DEVICE_ID");
		customFields.add(cf);

		cf = new ZDTicketCustomFields();
		cf.setId(ZendeskAttributes.EMAIL.getZendeskId());
		cf.setValue("EMAIL");
		customFields.add(cf);

		cf = new ZDTicketCustomFields();
		cf.setId(ZendeskAttributes.FACEBOOK_ID.getZendeskId());
		cf.setValue("FACEBOOK_ID");
		customFields.add(cf);

		cf = new ZDTicketCustomFields();
		cf.setId(ZendeskAttributes.IOS_TOKENS.getZendeskId());
		cf.setValue("IOS_TOKENS");
		customFields.add(cf);

		cf = new ZDTicketCustomFields();
		cf.setId(ZendeskAttributes.LANGUAGE.getZendeskId());
		cf.setValue("LANGUAGE");
		customFields.add(cf);

		cf = new ZDTicketCustomFields();
		cf.setId(ZendeskAttributes.LIKE.getZendeskId());
		cf.setValue(new Integer(5));
		customFields.add(cf);

		cf = new ZDTicketCustomFields();
		cf.setId(ZendeskAttributes.MODEL.getZendeskId());
		cf.setValue("model");
		customFields.add(cf);

		cf = new ZDTicketCustomFields();
		cf.setId(ZendeskAttributes.NAME.getZendeskId());
		cf.setValue("name");
		customFields.add(cf);

		cf = new ZDTicketCustomFields();
		cf.setId(ZendeskAttributes.NPS.getZendeskId());
		cf.setValue(new Integer(4));
		customFields.add(cf);

		cf = new ZDTicketCustomFields();
		cf.setId(ZendeskAttributes.ORIGIN.getZendeskId());
		cf.setValue("WEB");
		customFields.add(cf);

		cf = new ZDTicketCustomFields();
		cf.setId(ZendeskAttributes.SYSTEM_NAME.getZendeskId());
		cf.setValue("SYSTEM_NAME");
		customFields.add(cf);

		cf = new ZDTicketCustomFields();
		cf.setId(ZendeskAttributes.TIMEZONE.getZendeskId());
		cf.setValue("TIMEZONE");
		customFields.add(cf);

		cf = new ZDTicketCustomFields();
		cf.setId(ZendeskAttributes.USER_AGENT.getZendeskId());
		cf.setValue("USER_AGENT");
		customFields.add(cf);

		cf = new ZDTicketCustomFields();
		cf.setId(ZendeskAttributes.USER_ID.getZendeskId());
		cf.setValue("USER_ID");
		customFields.add(cf);

		ticket.setCustom_fields(customFields);

		ZDTicketCreateRequest ticketCreateRequest = new ZDTicketCreateRequest();
		ticketCreateRequest.setTicket(ticket);

		ZendeskClient.createTicket(ticketCreateRequest);

	}

}
