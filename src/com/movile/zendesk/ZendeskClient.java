/**
 * 
 */
package com.movile.zendesk;

import com.google.gson.Gson;
import com.movile.zendesk.bean.api.ZDTicketCreateRequest;
import com.movile.zendesk.bean.api.ZDUserCreateRequest;
import com.movile.zendesk.util.HttpUtils;

/**
 * @author marcosloiola1
 * 
 */
public class ZendeskClient {

	private static final String HOST = "https://docadvisor.zendesk.com/api/v2/";

	private static final String LOGIN = "marcos.loiola2@gmail.com";

	private static final String TOKEN = "docaws2013";

//	private static final String TOKEN = "zenDocAdvisor";

	// curl -v -u {email_address}:{password} \ -H
	// "Content-Type: application/json" -X POST -d '{"user": {"name":
	// "Roger Wilco", "email": "roge@example.org"}}'

	public static String createUser(ZDUserCreateRequest userCreateRequest) {

		String url = HOST + "users.json";

		System.out.println(new Gson().toJson(userCreateRequest));

		return HttpUtils.request(url, "POST", new Gson().toJson(userCreateRequest), LOGIN, TOKEN);

	}

	public static String createTicket(ZDTicketCreateRequest ticketCreateRequest) {

		String url = HOST + "tickets.json";

		System.out.println(new Gson().toJson(ticketCreateRequest));

		return HttpUtils.request(url, "POST", new Gson().toJson(ticketCreateRequest), LOGIN, TOKEN);

	}

}
