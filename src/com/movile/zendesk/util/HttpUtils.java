/**
 * 
 */
package com.movile.zendesk.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

import org.apache.commons.codec.binary.Base64;

/**
 * @author marcosloiola1
 * 
 */
public class HttpUtils {

	public static String request(String requestURL, String method, String data, String user, String token) {

		try {

			URL url = new URL(requestURL);
			HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
			httpConn.setRequestMethod(method);
			httpConn.setDoOutput(true);
			httpConn.setDoInput(true);
			httpConn.addRequestProperty("Content-Type", "application/json");

			String userpassword = user + ":" + token;
			String encodedAuthorization = new String(Base64.encodeBase64(userpassword.getBytes()));

			System.out.println(encodedAuthorization);

			httpConn.setRequestProperty("Authorization", "Basic " + encodedAuthorization);

			httpConn.connect();

			OutputStream os = httpConn.getOutputStream();

			os.write(data.getBytes());

			os.flush();
			os.close();

			InputStream is = httpConn.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));

			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

			return sb.toString();

		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

}
